<?php

require_once 'conexcion.php'; 

class GestorAreasYrutasM{

	public function setAreasModel($datosModel, $tabla){
		$stmt = Conexcion::Conectar()->prepare("INSERT INTO $tabla (nombre, descripcion, direccion, encargado, telefono) VALUES (:nombre, :descripcion, :direccion, :encargado, :telefono)");

		$stmt -> bindParam(":nombre", $datosModel["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":descripcion", $datosModel["descripcion"], PDO::PARAM_STR);
		$stmt -> bindParam(":direccion", $datosModel["direccion"], PDO::PARAM_STR);
		$stmt -> bindParam(":encargado", $datosModel["encargado"], PDO::PARAM_STR);
		$stmt -> bindParam(":telefono", $datosModel["telefono"], PDO::PARAM_STR);

		if ($stmt->execute()) {
			return "ok";
		}else{
			return "error";
		}

		$stmt -> close();
	}

	public function getAreasModel($tabla){
		$stmt = Conexcion::Conectar()->prepare("SELECT * FROM $tabla");
		$stmt->execute();
		return $stmt->fetchAll();
		$stmt->close();
	}
	public function updateAreaModel($datosModel, $tabla){
		$stmt = Conexcion::Conectar()->prepare("UPDATE $tabla SET nombre = :nombre, descripcion = :descripcion, direccion = :direccion, encargado = :encargado, telefono = :telefono WHERE id = :id ");
		$stmt -> bindParam(":nombre", $datosModel["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":descripcion", $datosModel["descripcion"], PDO::PARAM_STR);
		$stmt -> bindParam(":direccion", $datosModel["direccion"], PDO::PARAM_STR);
		$stmt -> bindParam(":encargado", $datosModel["encargado"], PDO::PARAM_STR);
		$stmt -> bindParam(":telefono", $datosModel["telefono"], PDO::PARAM_STR);
		$stmt -> bindParam(":id", $datosModel["id"], PDO::PARAM_INT);

		if ($stmt->execute()) {
			return "ok";
		}else{
			return "error";
		}

		$stmt -> close();
	}

	public function setRutaModel($datosModel, $tabla){
		$stmt = Conexcion::Conectar()->prepare("INSERT INTO $tabla (nombre, descripcion, idArea) VALUES (:nombre, :descripcion, :idArea)");

		$stmt->bindParam(":nombre", $datosModel["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":descripcion", $datosModel["descripcion"], PDO::PARAM_STR);
		$stmt->bindParam(":idArea", $datosModel["idArea"], PDO::PARAM_INT);

		if ($stmt->execute()) {
			return "ok";
		}else{
			return "error";
		}

		$stmt->close();
	}

	public function getRutasModel($tabla){
		$stmt = Conexcion::Conectar()->prepare("SELECT rutas.id, rutas.nombre, rutas.descripcion, areas.nombre as 'area' FROM $tabla INNER JOIN areas ON $tabla.idArea = areas.id");
		$stmt->execute();
		return $stmt->fetchAll();
		$stmt->clase();
	}

	public function updateRutaModel($datosModel, $tabla){
		$stmt = Conexcion::Conectar()->prepare("UPDATE $tabla SET nombre = :nombre, descripcion = :descripcion, idArea = :idArea WHERE id = :id ");

		$stmt->bindParam(":nombre", $datosModel["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":descripcion", $datosModel["descripcion"], PDO::PARAM_STR);
		$stmt->bindParam(":idArea", $datosModel["idArea"], PDO::PARAM_INT);
		$stmt->bindParam(":id", $datosModel["id"], PDO::PARAM_INT);

		
		if ($stmt->execute()) {
			return "ok";
		}else{
			return "error";
		}

		$stmt->close();
	}
}