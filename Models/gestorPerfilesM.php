<?php 

require_once 'conexcion.php';

class GestorPerfilesM{
	//TRAER PASSWORD PARA LOGIN
	public function loginModel($dato, $tabla){
		$stmt = Conexcion::Conectar()->prepare("SELECT password from $tabla WHERE username = '$dato'");
		$stmt -> execute();
		return $stmt->fetch();
		$stmt->close();
	}
	//TRAER DATOS DE USUARIO PARA VARIABLES DE SECCION
	public function getdataUserModel($dato, $tabla){
		$stmt = Conexcion::Conectar()->prepare("SELECT nombre, apellido, email, foto, username, rol, id from $tabla WHERE username = '$dato'");
		$stmt -> execute();
		return $stmt->fetchAll();
		$stmt->close();
	}
	//GUARDAR PERFIL EN LA BASE DE DATOS
	public function setPerfilModel($datosModel, $tabla){
		$stmt = Conexcion::Conectar()->prepare("INSERT INTO $tabla (nombre, apellido, username, password, email, foto, rol) VALUES (:nombre, :apellido, :username, :password, :email, :foto, :rol)");
		
		$stmt -> bindParam(":nombre", $datosModel["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":apellido", $datosModel["apellido"], PDO::PARAM_STR);
		$stmt -> bindParam(":email", $datosModel["email"], PDO::PARAM_STR);
		$stmt -> bindParam(":rol", $datosModel["rol"], PDO::PARAM_INT);
		$stmt -> bindParam(":username", $datosModel["username"], PDO::PARAM_STR);
		$stmt -> bindParam(":password", $datosModel["contraseña"], PDO::PARAM_STR);
		$stmt -> bindParam(":foto", $datosModel["foto"], PDO::PARAM_STR);
		
		if ($stmt->execute()) {
			return "ok";
		}else{
			return "error";
		}
		$stmt->close();

	}
	//GUARDAR IMAGEN DEL PERFIL ACTUAL UPDATE
	public function updateImagenPerfilModel($datosModel, $tabla){

		$stmt = Conexcion::Conectar()->prepare("UPDATE $tabla SET foto = :foto WHERE id = :id");
		
		$stmt -> bindParam(":foto", $datosModel["foto"], PDO::PARAM_STR);
		$stmt -> bindParam(":id", $datosModel["id"], PDO::PARAM_INT);

		if ($stmt->execute()) {
			return "ok";
		}else{
			return "error";
		}
		$stmt->close();
	}

	//ACTUALIZAR PERFIL 
	public function updatePerfilModel($datosModel, $tabla){
		$stmt = Conexcion::Conectar()->prepare("UPDATE $tabla SET nombre = :nombre, apellido = :apellido, username = :username, password = :password, email = :email, rol = :rol WHERE id = :id");

		$stmt -> bindParam(":nombre", $datosModel["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":apellido", $datosModel["apellido"], PDO::PARAM_STR);
		$stmt -> bindParam(":email", $datosModel["email"], PDO::PARAM_STR);
		$stmt -> bindParam(":rol", $datosModel["rol"], PDO::PARAM_INT);
		$stmt -> bindParam(":id", $datosModel["id"], PDO::PARAM_INT);
		$stmt -> bindParam(":username", $datosModel["username"], PDO::PARAM_STR);
		$stmt -> bindParam(":password", $datosModel["contraseña"], PDO::PARAM_STR);
	
		
		if ($stmt->execute()) {
			return "ok";
		}else{
			return "error";
		}
		$stmt->close();

	}

	//MOSTRAR LOS PERFILES
	public function viewsPerfilesModel($dato, $tabla){
		$stmt = Conexcion::Conectar()->prepare("SELECT nombre, apellido, rol, email, username, id FROM $tabla WHERE id <> '$dato'");
		$stmt -> execute();
		return $stmt->fetchAll();
		$stmt->close();
	}

	// ELIMINAR PERFIL 
	public function deletePerfilModel($dato, $tabla){
		$stmt = Conexcion::Conectar()->prepare("DELETE FROM $tabla WHERE id = '$dato'");
		if ($stmt->execute()) {
			return "ok";
		}else{
			return "error";
		}
		$stmt->close();
	}
}