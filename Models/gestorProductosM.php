<?php 

require_once 'conexcion.php';

class GestorProductosModel{

	public function setProductosModel($datosModel, $tabla){
		$stmt = Conexcion::Conectar()->prepare("INSERT INTO $tabla (nombre, descripcion, precioV, idArea, margen, numeroRojo) VALUES (:nombre, :descripcion, :precioV, :idArea, :margen , :numeroRojo) ");
		$stmt->bindParam(":nombre", $datosModel["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":descripcion", $datosModel["descripcion"], PDO::PARAM_STR);
		$stmt->bindParam(":precioV", $datosModel["precio"], PDO::PARAM_STR);
		$stmt->bindParam(":margen", $datosModel["margen"], PDO::PARAM_STR);
		$stmt->bindParam(":numeroRojo", $datosModel["rojos"], PDO::PARAM_INT);
		$stmt->bindParam(":idArea", $datosModel["idArea"], PDO::PARAM_INT);

		if ($stmt->execute()) {
			return "ok";
		}else{
			return "error";
		}

		$stmt->close();
	}

	public function getProductosModel($tabla){
		$stmt = Conexcion::Conectar()->prepare("SELECT productos.id, productos.numeroRojo, productos.nombre, productos.descripcion, productos.precioV, productos.existencias, productos.margen,(productos.precioV * productos.existencias) as 'inversion', ((productos.precioV * productos.existencias) * productos.margen) as 'ganancia', areas.nombre as 'area' FROM $tabla INNER JOIN areas ON productos.idArea = areas.id ");
		$stmt->execute();
		return $stmt->fetchAll();
		$stmt->close();
	}

	public function updateProductosModel($datosModel, $tabla){
		$stmt = Conexcion::Conectar()->prepare("UPDATE $tabla SET nombre = :nombre, descripcion = :descripcion, precioV = :precioV, idArea = :idArea, margen = :margen, numeroRojo = :numeroRojo WHERE id = :id ");
		$stmt->bindParam(":nombre", $datosModel["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":descripcion", $datosModel["descripcion"], PDO::PARAM_STR);
		$stmt->bindParam(":precioV", $datosModel["precio"], PDO::PARAM_STR);
		$stmt->bindParam(":margen", $datosModel["margen"], PDO::PARAM_STR);
		$stmt->bindParam(":numeroRojo", $datosModel["rojos"], PDO::PARAM_INT);
		$stmt->bindParam(":idArea", $datosModel["idArea"], PDO::PARAM_INT);
		$stmt->bindParam(":id", $datosModel["id"], PDO::PARAM_INT);

		if ($stmt->execute()) {
			return "ok";
		}else{
			return "error";
		}

		$stmt->close();
	}

	public function deleteProductosModel($datoModel, $tabla){
		$stmt = Conexcion::Conectar()->prepare("DELETE FROM $tabla WHERE id = '$datoModel'");

		if ($stmt->execute()){
			return "ok";
		}else{
			return "false";
		}
	}

	public function traerProductosParaGraficaModel($tabla){
		$stmt = Conexcion::Conectar()->prepare("SELECT nombre, id From $tabla");

		$stmt -> execute();
		return $stmt->fetchAll();
		$stmt->close();
	}
}