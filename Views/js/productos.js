$(document).ready(function($) {
if (window.location.href.indexOf("productos") > -1  ) {

	var nombre = $("#nombreProductoEditar");
	var descripcion = $("#descripcionProductoEditar");
	var precioV = $("#precioProductoEditar");
	var numRojo = $("#numRojoProductoEditar");
	var margen = $("#margenProductoEditar");
	var idProducto = $("#idProductoEditar");
	
	  
	$(".components li a").each(function(index) { 
		if ($(this).attr("href") == "productos") {
			$(this).parent().attr("class", "active");
		}
	});
   	
   	var ctx = document.getElementById('ventasProductosSemanal').getContext('2d');
	var myChart = new Chart(ctx, {
	    type: 'line',
	    data: {
	        labels: ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
	        datasets: [{
	            label: 'Garrafon',
	            data: [12, 19, 3, 5, 2, 3],
	            backgroundColor: [
	                'rgba(40, 167, 69, 0.5)',
	                'rgba(54, 162, 235, 1)',
	                'rgba(255, 206, 86, 1)',
	                'rgba(75, 192, 192, 1)',
	                'rgba(153, 102, 255, 1)',
	                'rgba(255, 159, 64, 1)'
		          
	            ],
	            borderColor: [
	                'rgba(0, 255, 58, 1)',
	                'rgba(54, 162, 235, 1)',
	                'rgba(255, 206, 86, 1)',
	                'rgba(75, 192, 192, 1)',
	                'rgba(153, 102, 255, 1)',
	                'rgba(255, 159, 64, 1)'
	        
	            ],
	            borderWidth: 1
	        }]
	    },

	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero: true
	                }
	            }]
	        }
	    }
	});

	var tableProducto = $('#tablaProductos').DataTable( {
		
        responsive: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
          
        }

    } );


    // editar productos 
    $('#tablaProductos').on( 'click', 'tbody tr', function () {

    	let classProducto = $(this).children('td').eq(8).children('button').attr('class');
    	let hrefProducto = $(this).children('td').eq(8).children('a').attr('href');
    	
    	if (typeof classProducto !== 'undefined') {
		  	//dos
	    	if (classProducto.length == 36) {
	    		var rojo = classProducto.slice(-2);
	    	//tres	
	    	}else if (classProducto.length == 37) {
	    		var rojo = classProducto.slice(-3);
	    	}else{
	    		var rojo = classProducto.slice(-1);
	    	}
		}
    	
		if (typeof hrefProducto !== 'undefined') {
			//uno
	    	if (hrefProducto.length == 43) {
	    		var id = hrefProducto.slice(-1);
	    	//dod	
	    	}else if (hrefProducto.length == 34) {
	    		var id = hrefProducto.slice(-2);
	    	}else{
	    		var id = hrefProducto.slice(-3);
	    	}
		}
    	


	    if ( tableProducto.row( this, { selected: true } ).any() ) {
	        // table.row(this).deselect();
	        
	        fila = tableProducto.row(this).data();
	        // console.log(fila);
	    
	       //  if (fila[5].length == 120) {
	      	// 	//id entre 10 y 99	
	       //  	idArea = fila[5].slice(60, 61);
	      				
	      	// }else if(fila[5].length == 277){
	      	// 	//id entre 1 y 9
	       //  	idArea = fila[5].slice(60, 60);
	        	
	      	// }else{
	      	// 	//id mas de 100
	       //  	idArea = fila[5].slice(60, 62);
	      		
	      	// }

	      	nombre.val(fila[0]);
	      	descripcion.val(fila[1]);
	      	precioV.val(fila[2].slice(1));
	      	margen.val(fila[4]);
	      	numRojo.val(rojo);
	      	idProducto.val(id);
	    
	    }
	    else {
	        // table.row( this ).select();
	    }
	} );	


}
});

function eliminarProducto(url){
	swal({
	  	title: "Estas seguro?",
	  	text: "Estas a punto de eliminar un Producto!",
	  	type: "warning",
	  	showCancelButton: true,
	  	confirmButtonClass: "btn-danger",
	  	confirmButtonText: "Si, lo estoy!",
	  	cancelButtonText: "No, lo estoy!",
	  	closeOnConfirm: false,
	  	closeOnCancel: false
	},
	function(isConfirm) {
	  	if (isConfirm) {
	    	swal("Eliminando!", "la informacion se esta eliminando.", "success");
			window.location = url;

	  	} else {
	    	swal("Cancelando", "la informacion no se eliminara :)", "error");
	  	}
	});	
}