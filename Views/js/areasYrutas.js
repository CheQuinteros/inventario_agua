$(document).ready(function($) {
if (window.location.href.indexOf("areasYrutas") > -1  ) {

	var nombreArea = $("#nombreAreaEditar");
	var direccionArea = $("#direccionAreaEditar");
	var telefonoArea = $("#telefonoAreaEditar");
	var descripcionArea = $("#descripcionAreaEditar");
	var encargadoArea = $("#encargadoAreaEditar");
	var AreaId = $("#AreaEditar");

	var nombreRuta = $("#nombreRutaEditar");
	var descripcionRuta = $("#descripcionRutaEditar");
	var Rutaid = $("#idRutaEditar");
	  
	$(".components li a").each(function(index) { 
		if ($(this).attr("href") == "areasYrutas") {
			$(this).parent().attr("class", "active");
		}
	});
    

	var tableAreas = $('#tablaAreas').DataTable( {
		
        responsive: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
          
        }

    } );

	var tableRutas = $('#tablaRutas').DataTable( {
	
	    responsive: true,
	    "language": {
	        "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
	      
	    }

    } );

    //EDITAR AREA
	$('#tablaAreas').on( 'click', 'tbody tr', function () {
	    if ( tableAreas.row( this, { selected: true } ).any() ) {
	        // table.row(this).deselect();
	        fila = tableAreas.row(this).data();

	        var idArea;
	        
	        if (fila[5].length == 120) {
	      		//id entre 10 y 99	
	        	idArea = fila[5].slice(60, 61);
	      				
	      	}else if(fila[5].length == 277){
	      		//id entre 1 y 9
	        	idArea = fila[5].slice(60, 60);
	        	
	      	}else{
	      		//id mas de 100
	        	idArea = fila[5].slice(60, 62);
	      		
	      	}
	      	AreaId.val(idArea);
	      	nombreArea.val(fila[0]);
			direccionArea.val(fila[1]);
			telefonoArea.val(fila[4]);
			descripcionArea.val(fila[2]);
			encargadoArea.val(fila[3]);
	    
	    }
	    else {
	        // table.row( this ).select();
	    }
	} );	

	 //EDITAR RUTA
	$('#tablaRutas').on( 'click', 'tbody tr', function () {
	    if ( tableRutas.row( this, { selected: true } ).any() ) {
	        // table.row(this).deselect();
	        fila = tableRutas.row(this).data();

	        var idRuta;
	 
	        
	        if (fila[3].length == 130) {
	      		//id entre 10 y 99	
	        	idRuta = fila[3].slice(60, 62);
	      				
	      	}else if(fila[3].length == 129){
	      		//id entre 1 y 9
	        	idRuta = fila[3].slice(60, 61);
	        	
	      	}else{
	      		//id mas de 100
	        	idRuta = fila[3].slice(60, 63);
	      		
	      	}
	    
	    	nombreRuta.val(fila[0]);
			descripcionRuta.val(fila[1]);
			Rutaid.val(idRuta);
	    
	    }
	    else {
	        // table.row( this ).select();
	    }
	} );		

}
});