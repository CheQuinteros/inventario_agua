$(document).ready(function() {


if (window.location.href.indexOf("perfiles") > -1  ) {
	// VARIABLES
	var nombreEditar = $("#nombrePerfilEditar");
	var apellidoEditar = $("#apellidoPerfilEditar");
	var emailEditar = $("#emailPerfilEditar");
	var usernameEditar = $("#usernamePerfilEditar");
	var idEditar = $("#idEditarPerfil");

	var filaNombre;
    var filaApellido;
    var filaEmial;
    var filaUsername;
    var filaId;
	// DATATABLE  ///////////////////////////////////////////////////////////////////////////
	var table = $('#tablaMostrarPerfiles').DataTable( {
		
        responsive: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
          
        }

    } );

    // new $.fn.dataTable.FixedHeader( table );

	
	// ICON RESPONSIVE //////////////////////////////////////////////////////////////////////
	if (window.innerWidth > 400 && window.innerWidth < 1024 ) {
		var elemento = $(".btnEliminarPerfil");
		elemento.html('<i class="fas fa-pencil-alt"></i>');

		var elemento = $(".btnActualizarPerfil");
		elemento.html('<i class="fas fa-user-edit"></i>');
	}

	// ACRUALIZAR SOLO FOTO //////////////////////////////////////////////////////////////////
    const inputImage = document.querySelector('#image');
    // Nodo donde estará el editor
    const editor = document.querySelector('#editor');
    // El canvas donde se mostrará la previa
    const miCanvas = document.querySelector('#preview');
    // Contexto del canvas
    const contexto = miCanvas.getContext('2d');
    // Ruta de la imagen seleccionada
    let urlImage = undefined;
    // Evento disparado cuando se adjunte una imagen
    inputImage.addEventListener('change', abrirEditor, false);

    /**
    * Método que abre el editor con la imagen seleccionada
    */
    function abrirEditor(e) {
        // Obtiene la imagen
        urlImage = URL.createObjectURL(e.target.files[0]);

        // Borra editor en caso que existiera una imagen previa
        editor.innerHTML = '';
        let cropprImg = document.createElement('img');
        cropprImg.setAttribute('id', 'croppr');
        editor.appendChild(cropprImg);

        // Limpia la previa en caso que existiera algún elemento previo
        contexto.clearRect(0, 0, miCanvas.width, miCanvas.height);

        // Envia la imagen al editor para su recorte
        document.querySelector('#croppr').setAttribute('src', urlImage);

        // Crea el editor
        new Croppr('#croppr', {
            aspectRatio: 1,
            startSize: [70, 70],
            onCropEnd: recortarImagen
        })
    }

    /**
    * Método que recorta la imagen con las coordenadas proporcionadas con croppr.js
    */
    function recortarImagen(data) {
        // Variables
        const inicioX = data.x;
        const inicioY = data.y;
        const nuevoAncho = data.width;
        const nuevaAltura = data.height;
        const zoom = 1;
        let imagenEn64 = '';
        // La imprimo
        miCanvas.width = nuevoAncho;
        miCanvas.height = nuevaAltura;
        // La declaro
        let miNuevaImagenTemp = new Image();
        // Cuando la imagen se carge se procederá al recorte
        miNuevaImagenTemp.onload = function() {
            // Se recorta
            contexto.drawImage(miNuevaImagenTemp, inicioX, inicioY, nuevoAncho * zoom, nuevaAltura * zoom, 0, 0, nuevoAncho, nuevaAltura);
            // Se transforma a base64
            imagenEn64 = miCanvas.toDataURL("image/jpeg");
            console.log(imagenEn64);
            document.querySelector("#urlImagenNueva").value = imagenEn64;
            // Mostramos el código generado
            // document.querySelector('#base64').textContent = imagenEn64;
            // document.querySelector('#base64HTML').textContent = '<img src="' + imagenEn64.slice(0, 40) + '...">';

        }
        // Proporciona la imagen cruda, sin editarla por ahora
        miNuevaImagenTemp.src = urlImage;
       
    }

    // EDITAR PERFIL ACTUAL /////////////////////////////////////////////////////
    
    $("#btnActualizarPerfilActual").on("click", function(){
    	idEditar.val("");
    	nombreEditar.val(localStorage.getItem("nombre"));
    	emailEditar.val(localStorage.getItem("email"));
    	apellidoEditar.val(localStorage.getItem("apellido"));
    	usernameEditar.val(localStorage.getItem("username"));
    	// idEditar.val(localStorage.getItem("identificador"));
    })
    // EDITAR PERFIL  /////////////////////////////////////////////////////
	$('#tablaMostrarPerfiles').on( 'click', 'tbody tr', function () {
	    if ( table.row( this, { selected: true } ).any() ) {
	        // table.row(this).deselect();
	        fila = table.row(this).data();
	        filaNombre = fila[0];
	        filaApellido = fila[1];
	        filaEmial = fila[2];
	        filaUsername = fila[3];
	        filaId = 0;
	      
	      	if (fila[5].length == 278) {
	      		//id entre 10 y 99	
	        	filaId = fila[5].slice(88, 90);
	      				
	      	}else if(fila[5].length == 277){
	      		//id entre 1 y 9
	        	filaId = fila[5].slice(88, 89);
	        	
	      	}else{
	      		//id mas de 100
	        	filaId = fila[5].slice(88, 91);
	      		
	      	}
	      	
	      	usernameEditar.val(filaUsername);
	    	idEditar.val(filaId);
	    	nombreEditar.val(filaNombre);
		    emailEditar.val(filaEmial);
		   	apellidoEditar.val(filaApellido);
	    }
	    else {
	        // table.row( this ).select();
	    }
	} );

	
 

}
});


function eliminar(){
      		swal({
		  	title: "Estas seguro?",
		  	text: "Estas a punto de eliminar a un usuario!",
		  	type: "warning",
		  	showCancelButton: true,
		  	confirmButtonClass: "btn-danger",
		  	confirmButtonText: "Si, lo estoy!",
		  	cancelButtonText: "No, lo estoy!",
		  	closeOnConfirm: false,
		  	closeOnCancel: false
		},
		function(isConfirm) {
		  	if (isConfirm) {
		    	swal("Eliminando!", "la informacion se esta eliminando.", "success");
	    		window.location = $("a.btnEliminarPerfil").attr("href");

		  	} else {
		    	swal("Cancelando", "la informacion no se eliminara :)", "error");
		  	}
		});	

}