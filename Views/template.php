<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Inventario Agua</title>
		<!-- Creados -->
	<link rel="stylesheet" href="Views/css/main.css">
	<link rel="stylesheet" href="Views/css/perfiles.css">
	<link rel="stylesheet" href="Views/css/productos.css">
	<link rel="stylesheet" href="Views/css/areasYrutas.css">

	<!-- JQUERY-UI -->
	<link rel="stylesheet" href="Views/css/jquery-ui/jquery-ui.min.css" />
	<link rel="stylesheet" href="Views/css/jquery-ui/jquery-ui.structure.min.css" />
	<link rel="stylesheet" href="Views/css/jquery-ui/jquery-ui.theme.min.css" />

	<!-- boostrap -->
	<link rel="stylesheet" href="Views/css/bootstrap-4.4.1/bootstrap.min.css">

	<!-- iconos -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/fontawesome.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/brands.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/solid.css">

	 <!-- dataTable -->
	<link rel="stylesheet" type="text/css" href="Views/css/dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="Views/css/fixedHeader.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="Views/css/responsive.dataTables.min.css">





	<script  src="Views/js/jquery-4.4.1.min.js"></script>
	<script  src="Views/js/bootstrap-4.4.1/bootstrap.min.js"></script>
	<!-- CORTAR IMAGENES -->
	<script src="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.min.js"></script>
	<link href="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.min.css" rel="stylesheet"/>

	<!-- JQUERY-UI -->
	<script type="text/javascript" src="Views/css/jquery-ui/jquery-ui.min.js"></script>
	<!-- dataTable -->
	<script type="text/javascript" charset="utf8" src="Views/js/dataTable/dataTables.min.js"></script>
	<script type="text/javascript" charset="utf8" src="Views/js/dataTable/dataTables.fixedHeader.min.js"></script>
	<script type="text/javascript" charset="utf8" src="Views/js/dataTable/dataTables.responsive.min.js"></script>

	<!-- Validacion -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
	
	<!-- Moments -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/es.js"></script>

	<script src="Views/js/sweetalert.min.js"></script>
	<link rel="stylesheet" href="Views/css/sweetalert.css">


</head>
<body>

<div class="container-fluid">
	<div class="row">
		  	
	  	<?php 
	  
	  		$enlaces = new GestorEnlacesC();
	  		$enlaces -> getPageController();
	  	 ?> 
	</div>
	
</div>










<script  src="Views/js/main.js"></script>
<script  src="Views/js/perfiles.js"></script>
<script  src="Views/js/inicio.js"></script>
<script  src="Views/js/productos.js"></script>
<script  src="Views/js/areasYrutas.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>

</body>
</html>