<div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-2 nopadding">
	<div class="wrapper" id="menu">
    <!-- MENU -->
    <nav id="sidebar">
	    <div class="sidebar-header">
	        <h3>Bienvenido</h3>
	        <span id="clickMenu" class="fas fa-bars"></span>
	    </div>
		
	    <ul class="list-unstyled components">
	        <p>Jose Porfirio</p>
	        <li>
	            <a href="inicio">Inicio</a>
	        </li>
	        <li>
	        	<a href="productos">Productos</a>
	        </li>
	        <li>
	          <a href="#">Ventas</a>
	        </li>
	        <li>
	          <a href="#">Proveedores</a>
	        </li>
	        <li>
	          <a href="#">Compras</a>
	        </li>
	        <li>
	          <a href="areasYrutas">Areas y Rutas</a>
	        </li>
	        <div class="ocultarMenu">
	        	<span  class="fas fa-angle-up"></span>
	        </div>
	     </ul>
	  
    </nav>

	</div>
</div>