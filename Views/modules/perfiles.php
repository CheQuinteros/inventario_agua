<?php
session_start();
if (!$_SESSION["login"]) {
	header("location:login");
	exit();
}

$Perfil = new GestorPerfilesC();

require_once 'menu.php';
?>

<div id="cabezera" class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-10">

<?php require_once 'cabezote.php'; ?> 	

	<div class="row" style="padding-top: 25px;">
	  	<div id="pagePerfiles" style="position: static;" class="col-12 col-sm-12 col-md-12 	col-lg-12 col-xl-12" >
			<div class="row">
				<div style="position: static;"  class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div id="titulo">
						<h3>Perfiles</h3>
					</div>
					<div id="contenedorIMG">
						<img src="<?php echo $_SESSION["foto"] ?>" alt="">
						<i id="editarSoloForoPerfil" data-toggle="modal" data-target="#modalActualizarFoto" class="fas fa-pencil-alt"></i>
					</div>
					<div id="datosPerfiles">
						<h3> <?php echo $_SESSION["username"] ?> </h3>
						<h3><?php echo $_SESSION["rol"] ?></h3>
						<button id="btnNuevoPerfil" type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModalLong">Nuevo</button>
						<button id="btnActualizarPerfilActual" type="button" class="btn btn-dark" data-toggle="modal" data-target="#modalEditarPerfil">Editar</button>
					</div>
				</div>
			</div>
			<div class="row" style="margin-top: 15px;">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div  >
						<table id="tablaMostrarPerfiles" class="table table-sm table-hover">
							<thead>
								<tr class="thead-dark">
									<th>Nombre</th>
									<th>Apellidos</th>
									<th>Email</th>
									<th>User</th>
									<th>Rol</th>
									<th class="action">Accion</th>
								</tr>
							</thead>
							<tbody>
								
								<?php $Perfil -> viewsPerfilesController();
									  // $Perfil -> deletePerfilController(); ?>
								
							</tbody>
						</table>
					</div>
					<div>
						<button style="margin-bottom: 20px;" id="btnImprimirTablaPerfiles" type="button" class="btn btn-success">Imprimir</button>
					</div>

				</div>
			</div>
	  	</div>
	</div>

</div>



<!-- MOTAL CREAR NUEVO PERFIL-->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Nuevo Perfil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" enctype="multipart/form-data">
			<div class="form-row">
				<div class="form-group col-md-6">
				    <label for="nombrePerfil">Nombre</label>
				    <input type="text" class="form-control" id="nombrePerfil" name="nombrePerfil" placeholder="Nombres" data-validation="alphanumeric" required>
				</div>
				<div class="form-group col-md-6">
				    <label for="apellidoPerfil">Apellido</label>
				    <input type="text" class="form-control" id="apellidoPerfil" name="apellidoPerfil" placeholder="Password" data-validation="alphanumeric" required>
				</div>
			</div>
 			<div class="form-row">
				<div class="form-group col-md-6">
				    <label for="usernamePerfil">Usuario</label>
				    <input type="text" class="form-control" id="usernamePerfil" name="usernamePerfil" placeholder="Usuario" data-validation="alphanumeric" required>
				</div>
				<div class="form-group col-md-6">
				    <label for="emailPerfil">Email</label>
				    <input type="text" class="form-control" id="emailPerfil" name="emailPerfil" placeholder="Ejemplo@gmail.com" data-validation="email" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
				    <label for="contraseñaPerfil">Contraseña</label>
				    <input type="password" class="form-control" id="contraseñaPerfil" name="contraseñaPerfil" placeholder="Contraseña" data-validation="alphanumeric" required>
				</div>
				<div class="form-group col-md-6">
				    <label for="rolPerfil">Rol</label>
		            <select id="rolPerfil" class="form-control" name="rolPerfil" required>
		                <option selected>--Selecciona--</option>
		                <option value="0">Administrador</option>
		                <option value="1">Editor</option>
		            </select>
				</div>
			</div>
			<div class="form-row">
				 <div class="input-group" style="padding: 0px 1%;">
					<div class="input-group-prepend">
					    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
					</div>
					<div class="custom-file">
					    <input type="file" class="custom-file-input" id="inputGroupFile01"
					       name="fotoPerfil">
					    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
					</div>
				</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>

<button type="submit" class="btn btn-success">Guardar</button>
</form>

      </div>
    </div>
  </div>
</div>

<?php $Perfil -> setPerfilController();?>

<!-- MODAL ACTUALIZAR IMAGEN DE PERFIL ACTUAL -->
<div class="modal fade" id="modalActualizarFoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable|modal-dialog-centered modal-dialog-scrollable|modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Actualizar foto de perfil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
 
			<div class="form-row">
				 <div class="input-group" style="padding: 0px 1%;">
					<div class="input-group-prepend">
					    <span class="input-group-text">Upload</span>
					</div>
					<div class="custom-file">
					    <input type="file" class="custom-file-input" id="image"
					       name="fotoPerfilActualActualizar">
					    <label class="custom-file-label" for="fotoPerfilActualActualizar">Choose file</label>
					</div>
				</div>
			</div>
			<!-- Editor donde se recortará la imagen con la ayuda de croppr.js -->
			<div style="width: 65%; margin: 0 auto; margin-top: 20px;" id="editor"></div>
			<!-- <p>3 Previsualiza el resultado</p> -->
			<!-- Previa del recorte -->
			<canvas style="width: 200px; height: 200px; margin: 0 auto; display: block; border-radius: 100px; margin-top:20px;" id="preview"></canvas>

			<!-- <h1>4 Resultado en Base64</h1> -->
			<!-- Muestra de la imagen recortada en Base64 -->
			<code id="base64"></code>
			
	<form action="" method="POST">
			<input type="hidden" id="urlImagenNueva" name="urlImagenNueva">	
			<input type="hidden" id="urlImagenAnterior" name="urlImagenAnterior" value="<?php echo $_SESSION["foto"]?>">	
			<input type="hidden" id="idPerfilActula" name="idPerfilActula" value="<?php echo $_SESSION["id"] ?>">	
			
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-success">Guardar</button>
    </form>
    <?php $Perfil -> updateImagenPerfilControlller();?>
      </div>
    </div>
  </div>
</div>


<!-- MODAL EDITAR PERFIL -->
<div class="modal fade" id="modalEditarPerfil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable|modal-dialog-centered modal-dialog-scrollable|modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal Editar Perfil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form method="POST" enctype="multipart/form-data">
         	<input type="hidden" value="" id="idEditarPerfil" name="idEditarPerfil">
			<div class="form-row">
				<div class="form-group col-md-6">
				    <label for="nombrePerfilEditar">Nombre</label>
				    <input type="text" class="form-control" id="nombrePerfilEditar" name="nombrePerfilEditar" placeholder="Nombres" data-validation="alphanumeric" required>
				</div>
				<div class="form-group col-md-6">
				    <label for="apellidoPerfilEditar">Apellido</label>
				    <input type="text" class="form-control" id="apellidoPerfilEditar" name="apellidoPerfilEditar" placeholder="Password" data-validation="alphanumeric" required>
				</div>
			</div>
 			<div class="form-row">
				<div class="form-group col-md-6">
				    <label for="usernamePerfilEditar">Usuario</label>
				    <input type="text" class="form-control" id="usernamePerfilEditar" name="usernamePerfilEditar" placeholder="Usuario" data-validation="alphanumeric" required>
				</div>
				<div class="form-group col-md-6">
				    <label for="emailPerfilEditar">Email</label>
				    <input type="text" class="form-control" id="emailPerfilEditar" name="emailPerfilEditar" placeholder="Ejemplo@gmail.com" data-validation="email" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
				    <label for="contraseñaPerfilEditar">Contraseña</label>
				    <input type="password" class="form-control" id="contraseñaPerfilEditar" name="contraseñaPerfilEditar" placeholder="Contraseña" data-validation="alphanumeric" required>
				</div>
				<div class="form-group col-md-6">
				    <label for="rolPerfilEditar">Rol</label>
		            <select id="rolPerfilEditar" class="form-control" name="rolPerfilEditar" required>
		                <option selected>--Selecciona--</option>
		                <option value="0">Administrador</option>
		                <option value="1">Editor</option>
		            </select>
				</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>

<button type="submit" class="btn btn-success">Guardar</button>
</form>
<?php 
	$Perfil -> updatePerfilController();
 ?>
      </div>
    </div>
  </div>
</div>