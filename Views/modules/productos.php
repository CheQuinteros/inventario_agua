<?php
session_start();
if (!$_SESSION["login"]) {
	header("location:login");
	exit();
}

$Area = new GestorAreasYrutasC();
$producto = new GestorProductosController();

require_once 'menu.php';
?>

<div id="cabezera" class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-10">

<?php require_once 'cabezote.php'; ?> 	

	<div class="row" style="padding-top: 25px;">
	  	<div id="pageProductos" style="position: static;" class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" >
			<div class="row" style="position: static;">
				<div style="position: static;" class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 infoProductos">
					<div id="btnActionProductos">
						<button type="button" class="btn btn-success btn-lg productoBtn" data-toggle="modal" data-target="#modalNuevoProducto">Nuevo</button>
						<button type="button" class="btn btn-dark btn-lg productoBtn">Imprimir</button>
					</div>
					<div>
						<div id="productosEnRojo">
							<h3>Productos en Rojo</h3>
							 <div id="listaProductosRojo">
								<ol>
<li title="Producto: Garrafon
Cnt.Minima: 20
Comprar: 100">producto  <span>444</span></li>

<li title="Producto: Garrafon
Cnt.Minima: 20
Comprar: 100">producto  <span>444</span></li>

<li title="Producto: Garrafon
Cnt.Minima: 20
Comprar: 100">producto  <span>444</span></li>

<li title="Producto: Garrafon
Cnt.Minima: 20
Comprar: 100">producto  <span>444</span></li>

<li title="Producto: Garrafon
Cnt.Minima: 20
Comprar: 100">producto  <span>444</span></li>

								</ol>
							</div>
							

						</div>
						
					</div>
				</div>
				<div style="position: static;" class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 infoProductos">
				
					<div class="form-group">
			            <select id="selecionDeProductoGrafica" class="form-control" name="selecionDeProductoGrafica" required>
			                <option selected>--Selecciona--</option>
			                	<?php $producto->traerProductosParaGraficaController(); ?>
			            </select>
					</div>
					<canvas id="ventasProductosSemanal" ></canvas>
				</div>
			</div>
			<div class="row" style="margin-top: 25px; position: static;">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div  >
						<table id="tablaProductos" class="table table-sm table-hover">
							<thead>
								<tr class="thead-dark">
									<th>Nombre</th>
									<th>Descripción</th>
									<th>Precio/V</th>
									<th>Existencias</th>
									<th>Margen</th>
									<th>Inversion</th>
									<th>Ganancia</th>
									<th>Area</th>
									<th class="action">Accion</th>
								</tr>
							</thead>
							<tbody>
								<?php $producto->getProductosController();
										$producto->deleteProductosController(); ?>
								
							</tbody>
						</table>
					</div>
					<div>
						<button style="margin-bottom: 20px;" id="btnImprimirTablaProductos" type="button" class="btn btn-success">Imprimir</button>
					</div>

				</div>
			</div>
	  	</div>
	</div>

</div>



<!-- Modal -->
<div class="modal fade" id="modalNuevoProducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable|modal-dialog-centered modal-sm|modal-lg|modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Nuevo Producto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="nombreProducto">Nombre</label>
              <input type="text" class="form-control" id="nombreProducto" name="nombreProducto" placeholder="Nombre" data-validation="custom" data-validation-regexp="^([a-zA-Z0-9 ]+)$" required>
            </div>
            <div class="form-group col-md-6">
              <label for="descripcionProducto">Descripción</label>
              <input type="text" class="form-control" id="descripcionProducto" name="descripcionProducto" placeholder="descripción" data-validation="custom" data-validation-regexp="^([a-zA-Z0-9 ]+)$" required>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="precioProducto">Precio/V</label>
              <input type="text" class="form-control" id="precioProducto" name="precioProducto" placeholder="00.00" data-validation="custom" data-validation-regexp="^([0-9 .]+)$" required>
            </div>
            <div class="form-group col-md-4">
              <label for="areaProducto">Area</label>
              <select id="areaProducto" name="areaProducto" class="form-control">
                <option selected>--Seleccionar--</option>
                <?php $Area -> getAreasParaRutasController(); ?>
              </select>
            </div>
            <div class="form-group col-md-2">
              <label for="margenProducto">Margen</label>
              <input type="text" class="form-control" id="margenProducto" name="margenProducto" placeholder="00.00" data-validation="custom" data-validation-regexp="^([0-9 .]+)$" required>
            </div>
            <div class="form-group col-md-2">
              <label for="numRojoProducto">Rojos</label>
              <input type="text" class="form-control" id="numRojoProducto" name="numRojoProducto" placeholder="000" data-validation="custom" data-validation-regexp="^([0-9]+)$" required>
            </div>
          </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-success">Guardar</button>
      </div>
 </form>
    </div>
  </div>
</div>
<?php $producto->setProductosController(); ?>


<!-- Modal -->
<div class="modal fade" id="modalEditarProducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable|modal-dialog-centered modal-sm|modal-lg|modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Editar Producto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST">
        	<input type="hidden" id="idProductoEditar" name="idProductoEditar">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="nombreProductoEditar">Nombre</label>
              <input type="text" class="form-control" id="nombreProductoEditar" name="nombreProductoEditar" placeholder="Nombre" data-validation="custom" data-validation-regexp="^([a-zA-Z0-9 ]+)$" required>
            </div>
            <div class="form-group col-md-6">
              <label for="descripcionProductoEditar">Descripción</label>
              <input type="text" class="form-control" id="descripcionProductoEditar" name="descripcionProductoEditar" placeholder="descripción" data-validation="custom" data-validation-regexp="^([a-zA-Z0-9 ]+)$" required>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="precioProductoEditar">Precio/V</label>
              <input type="text" class="form-control" id="precioProductoEditar" name="precioProductoEditar" placeholder="00.00" data-validation="custom" data-validation-regexp="^([0-9 .]+)$" required>
            </div>
            <div class="form-group col-md-4">
              <label for="areaProductoEditar">Area</label>
              <select id="areaProductoEditar" name="areaProductoEditar" class="form-control">
                <option selected>--Seleccionar--</option>
                <?php $Area -> getAreasParaRutasController(); ?>
              </select>
            </div>
            <div class="form-group col-md-2">
              <label for="margenProductoEditar">Margen</label>
              <input type="text" class="form-control" id="margenProductoEditar" name="margenProductoEditar" placeholder="00.00" data-validation="custom" data-validation-regexp="^([0-9 .]+)$" required>
            </div>
            <div class="form-group col-md-2">
              <label for="numRojoProductoEditar">Rojos</label>
              <input type="text" class="form-control" id="numRojoProductoEditar" name="numRojoProductoEditar" placeholder="000" data-validation="custom" data-validation-regexp="^([0-9]+)$" required>
            </div>
          </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-success">Guardar</button>
      </div>
 </form>
    </div>
  </div>
</div>
<?php $producto->updateProductosController(); ?>