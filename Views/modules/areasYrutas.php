<?php
session_start();
if (!$_SESSION["login"]) {
	header("location:login");
	exit();
}

require_once 'menu.php';
$area = new GestorAreasYrutasC();
?>

<div id="cabezera" class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-10">

<?php require_once 'cabezote.php'; ?> 	

	<div class="row" style="padding-top: 25px;">
	  	<div id="pagePerfiles" style="position: static;" class="col-12 col-sm-12 col-md-12 	col-lg-12 col-xl-12" >
			<div class="row areaContenedor">
				<div style="position: static;"  class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div style="position: static;" class="card">
						<div class="card-header AyRcabeza" >
						    <span>Areas</span>
							<?php $area -> getTotalAreasController(); ?>

						</div>
						<div class="card-body">
					    	<div class="row" style="margin-top: 25px; position: static;">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div  >
										<table id="tablaAreas" class="table table-sm table-hover">
											<thead>
												<tr class="thead-dark">
													<th>Nombre</th>
													<th>Dirección</th>
													<th>Descripción</th>
													<th>Encargado</th>
													<th>Telefono</th>
													<th class="actionArea">Accion</th>
												</tr>
											</thead>
											<tbody>
												<?php $area -> getAreasController(); ?>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						 </div>
					</div>

				</div>
			</div>
			<div class="row">
			
				<div style="position: static;"  class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div style="position: static;" class="card">
						<div class="card-header AyRcabeza" >
						    <span>Rutas</span>
						    <?php $area -> getTotalRutasController(); ?>
						</div>
						<div class="card-body">
						    <div class="row" style="margin-top: 25px; position: static;">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div  >
										<table id="tablaRutas" class="table table-sm table-hover">
											<thead>
												<tr class="thead-dark">
													<th>Nombre</th>
													<th>Descripcion</th>
													<th>Area</th>
					
													<th class="actionRuta">Accion</th>
												</tr>
											</thead>
											<tbody>
												<?php $area -> getRutasController(); ?>
													
											</tbody>
										</table>
									</div>
								</div>
							</div>
						 </div>
					</div>
	
				</div>

			</div>
		
	  	</div>
	</div>

</div>



<!-- MODAL CREAR NUEVA AREA-->
<div class="modal fade" id="nuevaArea" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Nueva Area</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" enctype="multipart/form-data">
			<div class="form-row">
				<div class="form-group col-md-6">
				    <label for="nombreArea">Nombre</label>
				    <input type="text" class="form-control" id="nombreArea" name="nombreArea" placeholder="Nombre" data-validation="custom" data-validation-regexp="^([a-zA-Z0-9 ]+)$" required>
				</div>
				<div class="form-group col-md-6">
				    <label for="direccionArea">Dirección</label>
				    <input type="text" class="form-control" id="direccionArea" name="direccionArea" placeholder="Direccion" data-validation="custom" data-validation-regexp="^([a-zA-Z0-9 ]+)$" required>
				</div>
			</div>
 			<div class="form-row">
				<div class="form-group col-md-6">
				    <label for="encargadoArea">Encargado</label>
				    <input type="text" class="form-control" id="encargadoArea" name="encargadoArea" placeholder="Encargado" data-validation="custom" data-validation-regexp="^([a-zA-Z0-9 ]+)$" required>
				</div>
				<div class="form-group col-md-6">
				    <label for="telefonoArea">Telefono</label>
				    <input type="text" class="form-control" id="telefonoArea" name="telefonoArea" placeholder="XXXX-XXXX" data-validation="custom" data-validation-regexp="^([0-9 -]+)$" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
				    <label for="descripcionArea">Descripción</label>
				    <input type="text" class="form-control" id="descripcionArea" name="descripcionArea" placeholder="Direccion" data-validation="custom" data-validation-regexp="^([a-zA-Z0-9 ]+)$" required>
				</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>

<button type="submit" class="btn btn-success">Guardar</button>
</form>

      </div>
    </div>
  </div>
</div>
<?php $area -> setAreasController(); ?>

<!-- MODAL EDITAR AREA-->
<div class="modal fade" id="modalEditarArea" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Editar Area</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" enctype="multipart/form-data">
        	<input type="hidden" id="AreaEditar" name="AreaEditar">
			<div class="form-row">
				<div class="form-group col-md-6">
				    <label for="nombreAreaEditar">Nombre</label>
				    <input type="text" class="form-control" id="nombreAreaEditar" name="nombreAreaEditar" placeholder="Nombre" data-validation="custom" data-validation-regexp="^([a-zA-Z0-9 ]+)$" required>
				</div>
				<div class="form-group col-md-6">
				    <label for="direccionAreaEditar">Dirección</label>
				    <input type="text" class="form-control" id="direccionAreaEditar" name="direccionAreaEditar" placeholder="Direccion" data-validation="custom" data-validation-regexp="^([a-zA-Z0-9 ]+)$" required>
				</div>
			</div>
 			<div class="form-row">
				<div class="form-group col-md-6">
				    <label for="encargadoAreaEditar">Encargado</label>
				    <input type="text" class="form-control" id="encargadoAreaEditar" name="encargadoAreaEditar" placeholder="Encargado" data-validation="custom" data-validation-regexp="^([a-zA-Z0-9 ]+)$" required>
				</div>
				<div class="form-group col-md-6">
				    <label for="telefonoAreaEditar">Telefono</label>
				    <input type="text" class="form-control" id="telefonoAreaEditar" name="telefonoAreaEditar" placeholder="XXXX-XXXX" data-validation="custom" data-validation-regexp="^([0-9 -]+)$" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
				    <label for="descripcionAreaEditar">Descripción</label>
				    <input type="text" class="form-control" id="descripcionAreaEditar" name="descripcionAreaEditar" placeholder="Direccion" data-validation="custom" data-validation-regexp="^([a-zA-Z0-9 ]+)$" required>
				</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>

<button type="submit" class="btn btn-success">Guardar</button>
</form>

      </div>
    </div>
  </div>
</div>
<?php $area -> updateAreaController(); ?>

<!-- MODAL CREAR NUEVA RUTA-->
<div class="modal fade" id="nuevaRuta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Nueva Ruta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" >
			<div class="form-row">
				<div class="form-group col-md-12">
				    <label for="nombreRuta">Nombre</label>
				    <input type="text" class="form-control" id="nombreRuta" name="nombreRuta" placeholder="Nombre" data-validation="custom" data-validation-regexp="^([a-zA-Z0-9 ]+)$" required>
				</div>
			</div>
 		
			<div class="form-row">
				<div class="form-group col-md-6">
				    <label for="descripcionRuta">Descripción</label>
				    <input type="text" class="form-control" id="descripcionRuta" name="descripcionRuta" placeholder="Descripcion" data-validation="custom" data-validation-regexp="^([a-zA-Z0-9 ]+)$" required>
				</div>
				<div class="form-group col-md-6">
				    <label for="areaRuta">Rol</label>
		            <select id="areaRuta" class="form-control" name="areaRuta" required>
		                <option selected>--Selecciona--</option>
		                <?php $area -> getAreasParaRutasController(); ?>
		            </select>
				</div>
			</div>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>

<button type="submit" class="btn btn-success">Guardar</button>
</form>

      </div>
    </div>
  </div>
</div>
<?php $area -> setRutaController(); ?>

<!-- MODAL EDITAR RUTA-->
<div class="modal fade" id="modalEditarRuta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Editar Ruta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" >
        	<input type="hidden" id="idRutaEditar" name="idRutaEditar">
			<div class="form-row">
				<div class="form-group col-md-12">
				    <label for="nombreRutaEditar">Nombre</label>
				    <input type="text" class="form-control" id="nombreRutaEditar" name="nombreRutaEditar" placeholder="Nombre" data-validation="custom" data-validation-regexp="^([a-zA-Z0-9 ]+)$" required>
				</div>
			</div>
 		
			<div class="form-row">
				<div class="form-group col-md-6">
				    <label for="descripcionRutaEditar">Descripción</label>
				    <input type="text" class="form-control" id="descripcionRutaEditar" name="descripcionRutaEditar" placeholder="Descripcion" data-validation="custom" data-validation-regexp="^([a-zA-Z0-9 ]+)$" required>
				</div>
				<div class="form-group col-md-6">
				    <label for="areaRutaEditar">Rol</label>
		            <select id="areaRutaEditar" class="form-control" name="areaRutaEditar" required>
		                <option selected>--Selecciona--</option>
		                <?php $area -> getAreasParaRutasController(); ?>
		            </select>
				</div>
			</div>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>

<button type="submit" class="btn btn-success">Guardar</button>
</form>

      </div>
    </div>
  </div>
</div>
<?php $area -> updateRutaController(); ?>
