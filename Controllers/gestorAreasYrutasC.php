<?php 

class GestorAreasYrutasC {
	// AREAS
	public function setAreasController(){

		if (isset($_POST["nombreArea"]) &&
			isset($_POST["direccionArea"]) &&
			isset($_POST["encargadoArea"]) &&
			isset($_POST["telefonoArea"]) &&
			isset($_POST["descripcionArea"])) {
			
			if (preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["nombreArea"]) &&
				preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["descripcionArea"]) &&
				preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["direccionArea"]) &&
				preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["encargadoArea"]) &&
				preg_match('/^[0-9 -]*$/', $_POST["telefonoArea"])) {
				
				$datosController = array('nombre' => $_POST["nombreArea"],
										'direccion' => $_POST["direccionArea"],
										'encargado' => $_POST["encargadoArea"],
										'telefono' => $_POST["telefonoArea"],
										'descripcion' => $_POST["descripcionArea"]);

				$response = GestorAreasYrutasM::setAreasModel($datosController, "areas");

				if ($response == "ok") {
					echo '
					<script>
						 swal({
                                title: "ok",
                                text: "Area Registrada Correctamente!",
                                type: "success",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "areasYrutas";
                                } 
                            });


					</script>

            	';
				}else{
						echo '
					<script>
						 swal({
                                title: "Error",
                                text: "Area NO Registrada!",
                                type: "danger",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "areasYrutas";
                                } 
                            });


					</script>

            	';
				}

			}
		}
	}

	public function getAreasController(){
		$response = GestorAreasYrutasM::getAreasModel("areas");
		foreach ($response as $row => $item) {
			// <a class="btn btn-danger btnEliminarArea" href="index.php?action=areasYrutas&deleteArea='.$item["id"].'" onclick="eliminar();return false;">Eliminar</a>

			echo '   
				<tr>
					<td>'.$item["nombre"].'</td>
					<td>'.$item["direccion"].'</td>
					<td>'.$item["descripcion"].'</td>
					<td>'.$item["encargado"].'</td>
					<td>'.$item["telefono"].'</td>
					<td>
						<button type="button" class="btn btn-dark btnActualizarArea '.$item["id"].'" data-toggle="modal" data-target="#modalEditarArea">Editar</button> 
					</td>
				</tr>


			';
		}
	}

	public function getTotalAreasController(){
		$response = GestorAreasYrutasM::getAreasModel("areas");
		// echo count($response);
		if (count($response) < 2) {		    
			echo '<button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#nuevaArea">Nueva</button>';
		}
	}

	public function updateAreaController(){
		if (isset($_POST["nombreAreaEditar"]) &&
			isset($_POST["direccionAreaEditar"]) &&
			isset($_POST["encargadoAreaEditar"]) &&
			isset($_POST["telefonoAreaEditar"]) &&
			isset($_POST["descripcionAreaEditar"])) {
			
			if (preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["nombreAreaEditar"]) &&
				preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["descripcionAreaEditar"]) &&
				preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["direccionAreaEditar"]) &&
				preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["encargadoAreaEditar"]) &&
				preg_match('/^[0-9 -]*$/', $_POST["telefonoAreaEditar"])) {
				
				$datosController = array('nombre' => $_POST["nombreAreaEditar"],
										'direccion' => $_POST["direccionAreaEditar"],
										'encargado' => $_POST["encargadoAreaEditar"],
										'telefono' => $_POST["telefonoAreaEditar"],
										'id' => $_POST["AreaEditar"],
										'descripcion' => $_POST["descripcionAreaEditar"]);

				$response = GestorAreasYrutasM::updateAreaModel($datosController, "areas");

				if ($response == "ok") {
					echo '
					<script>
						 swal({
                                title: "ok",
                                text: "Area Actualizada Correctamente!",
                                type: "success",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "areasYrutas";
                                } 
                            });


					</script>

            	';
				}else{
						echo '
					<script>
						 swal({
                                title: "Error",
                                text: "Area NO Actualizada!",
                                type: "danger",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "areasYrutas";
                                } 
                            });


					</script>

            	';
				}

			}
		}
	}


	// RUTAS
	public function getAreasParaRutasController(){
		$response = GestorAreasYrutasM::getAreasModel("areas");
		foreach ($response as $row => $item) {
			// <a class="btn btn-danger btnEliminarArea" href="index.php?action=areasYrutas&deleteArea='.$item["id"].'" onclick="eliminar();return false;">Eliminar</a>

			echo '   
                <option value="'.$item["id"].'">'.$item["nombre"].'</option>
			';
		}
	}

	public function setRutaController(){

		if (isset($_POST["nombreRuta"]) && isset($_POST["descripcionRuta"]) && isset($_POST["areaRuta"])) {

			if (preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["nombreRuta"]) &&
				preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["descripcionRuta"]) &&
				preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["areaRuta"])) {
				
				$datosController = array('nombre' => $_POST["nombreRuta"] ,
										'descripcion' => $_POST["descripcionRuta"] ,
										'idArea' => $_POST["areaRuta"]  );

				$response = GestorAreasYrutasM::setRutaModel($datosController, "rutas");

				if ($response == "ok") {
					echo '
					<script>
						 swal({
                                title: "ok",
                                text: "Ruta Registrada Correctamente!",
                                type: "success",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "areasYrutas";
                                } 
                            });


					</script>

            		';
				}else{
					echo '
					<script>
						 swal({
                                title: "Error",
                                text: "Ruta NO Registrada!",
                                type: "danger",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "areasYrutas";
                                } 
                            });


					</script>

            		';
				}
			}
		}
	}

	public function getRutasController(){
		$response = GestorAreasYrutasM::getRutasModel("rutas");
		foreach ($response as $row => $item) {
			echo '
				<tr>
					<td>'.$item["nombre"].'</td>
					<td>'.$item["descripcion"].'</td>
					<td>'.$item["area"].'</td>
					<td>
						<button type="button" class="btn btn-dark btnActualizarRuta '.$item["id"].'" data-toggle="modal" data-target="#modalEditarRuta">Editar</button> 
					</td>
				</tr>
				
			';
		}
	}

	public function getTotalRutasController(){
		$response = GestorAreasYrutasM::getRutasModel("rutas");
		if (count($response) < 10) {		    
			echo '<button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#nuevaRuta">Nueva</button>';
		}
	}

	public function updateRutaController(){
		
		if (isset($_POST["nombreRutaEditar"]) && isset($_POST["descripcionRutaEditar"]) && isset($_POST["areaRutaEditar"])) {

			if (preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["nombreRutaEditar"]) &&
				preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["descripcionRutaEditar"]) &&
				preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["areaRutaEditar"])) {
				
				$datosController = array('nombre' => $_POST["nombreRutaEditar"] ,
										'descripcion' => $_POST["descripcionRutaEditar"] ,
										'idArea' => $_POST["areaRutaEditar"],
										'id' => $_POST["idRutaEditar"]  );

				$response = GestorAreasYrutasM::updateRutaModel($datosController, "rutas");

				if ($response == "ok") {
					echo '
					<script>
						 swal({
                                title: "ok",
                                text: "Ruta Actualizada Correctamente!",
                                type: "success",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "areasYrutas";
                                } 
                            });


					</script>

            		';
				}else{
					echo '
					<script>
						 swal({
                                title: "Error",
                                text: "Ruta NO Actualizada!",
                                type: "danger",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "areasYrutas";
                                } 
                            });


					</script>

            		';
				}
			}
		}
	}
}

