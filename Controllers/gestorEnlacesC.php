<?php 

Class GestorEnlacesC{
	public function getTemplate(){
		include "Views/template.php";
	}
	public function getPageController(){

		if (isset($_GET["action"])) {
			$pageController = $_GET["action"];
		}else{
			$pageController = "index";
		}
		$response = GestorEnlacesM::getPageModel($pageController); 

		include $response;
	}
	
}