<?php
// session_start(); 

class GestorPerfilesC{

	public function loginController(){

		if (isset($_POST["userName"]) && isset($_POST["userPass"]) ) {

			if (preg_match('/^[a-zA-Z0-9]*$/' , $_POST["userPass"]) &&
				preg_match('/^[a-zA-Z0-9]*$/' , $_POST["userName"])) {

				$encriptar = crypt($_POST["userPass"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

				$getPass = GestorPerfilesM::loginModel($_POST["userName"], "users");

				if ($getPass["password"] == $encriptar) {

					session_start();
					$_SESSION["login"] = true;

					$dataUser = GestorPerfilesM::getdataUserModel($_POST["userName"], "users");

					foreach ($dataUser as $row => $item) {
						if ($item["rol"] == 0) {
							$rol = "Administrador";
						}else{
							$rol = "Editor";
						}
						$_SESSION["nombre"] = $item["nombre"];
						$_SESSION["apellido"] = $item["apellido"];
						$_SESSION["username"] = $item["username"];
						$_SESSION["rol"] = $rol;
						$_SESSION["foto"] = $item["foto"];
						$_SESSION["email"] = $item["email"];
						$_SESSION["id"] = $item["id"];
					}

					echo '
						<script>
				
							if (typeof(Storage) !== "undefined") {
								console.log("esta disponible");
							}else{
								console.log("no esta disponible");
							}
							localStorage.setItem("nombre", "'.$_SESSION["nombre"].'");
							localStorage.setItem("apellido", "'.$_SESSION["apellido"].'");
							localStorage.setItem("username", "'.$_SESSION["username"].'");
							localStorage.setItem("identificador", "'.$_SESSION["id"].'");
							localStorage.setItem("email", "'.$_SESSION["email"].'");
					
							window.location = "inicio";
						</script>
					';

				
				}else{
					echo '<div id="loginDatosMalos">
						<div class="alert alert-danger" role="alert">
						  El Usuario y Contraseña son incorrectos.
						</div>
					</div>';
			}
			}
		}
	}

	//GUARDAR PERFIL NUEVO
	public function setPerfilController(){
		
		$ruta = "";
	
		if (isset($_POST["nombrePerfil"]) && isset($_POST["apellidoPerfil"]) && 
			isset($_POST["emailPerfil"]) && isset($_POST["contraseñaPerfil"]) && 
			isset($_POST["usernamePerfil"]) && isset($_POST["rolPerfil"]) ) {


			if($_FILES["fotoPerfil"]["tmp_name"] != ""){

				$imagen = $_FILES["fotoPerfil"]["tmp_name"];
				$aleatorio = mt_rand(100, 999);
				$ruta = "Views/images/perfiles/perfil".$aleatorio.".jpg";
				$origen = imagecreatefromjpeg($imagen);
				// $destino = imagecrop($origen, ["x"=>0, "y"=>0, "width"=>1024, "height"=>1024]);
				imagejpeg($origen, $ruta);
			}

			if($ruta == ""){
                $ruta = "Views/images/perfiles/Default_User.jpg";	
            }
		
			if (preg_match('/^[a-zA-Z0-9]*$/', $_POST["nombrePerfil"]) && 
				preg_match('/^[a-zA-Z0-9]*$/', $_POST["apellidoPerfil"]) && 
				preg_match('/^[a-zA-Z0-9]*$/', $_POST["usernamePerfil"]) && 
				preg_match('/^[a-zA-Z0-9]*$/', $_POST["rolPerfil"]) && 
				preg_match('/^[a-zA-Z0-9]*$/', $_POST["contraseñaPerfil"]) && 
				preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/' , $_POST["emailPerfil"]) ) {

				

				$encriptar = crypt($_POST["contraseñaPerfil"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

				$datosController = array('nombre' => $_POST["nombrePerfil"] ,
										'apellido' => $_POST["apellidoPerfil"] ,
										'email' => $_POST["emailPerfil"] ,
										'username' => $_POST["usernamePerfil"] ,
										'rol' => $_POST["rolPerfil"] ,
										'contraseña' => $encriptar ,
										'foto' => $ruta  );
				$response = GestorPerfilesM::setPerfilModel($datosController, "users");
				
			    if ($response == "ok") {
            		echo '
					<script>
						 swal({
                                title: "ok",
                                text: "Perfil Registrado Correctamente!",
                                type: "success",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "perfiles";
                                } 
                            });


					</script>

            	';
	            }else{
	            	echo '
					<script>
						 swal({
                                title: "Error",
                                text: "Por favor trate nuevamente!",
                                type: "success",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "perfiles";
                                } 
                            });


					</script>

            	';
	            }

			}

		}
	}

	//ACTUALIZAR SOLO IMAGEN DEL PERFIL ACTUAL
	public function updateImagenPerfilControlller(){
		if (isset($_POST["urlImagenNueva"])) {

			$url    = $_POST["urlImagenNueva"];
			$aleatorio = mt_rand(100, 999);
			$img = "Views/images/perfiles/perfil".$aleatorio.".jpg";
			$file   = file($url);
			$result = file_put_contents($img, $file);


			if($_POST["urlImagenAnterior"] != "Views/images/perfiles/Default_User.jpg"){
                unlink($_POST["urlImagenAnterior"]);	
            }

            $datosController = array('foto' => $img, 
            						'id' => $_POST["idPerfilActula"] );

            $response = GestorPerfilesM::updateImagenPerfilModel($datosController, "users");

            if ($response == "ok") {
            	session_start();
            	$_SESSION["foto"] = $img;
            	echo '
					<script>
						 swal({
                                title: "ok",
                                text: "Imagen Actualizada Correctamente!",
                                type: "success",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "perfiles";
                                } 
                            });


					</script>

            	';
            }else{
            	echo '
					<script>
						 swal({
                                title: "Error",
                                text: "Por favor trate nuevamente!",
                                type: "success",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "perfiles";
                                } 
                            });


					</script>

            	';
            }
		}
	}

	//ACTUALIZAR DATOS DE PERFIL 
	public function updatePerfilController(){

	
		if (isset($_POST["nombrePerfilEditar"]) && isset($_POST["apellidoPerfilEditar"]) && 
			isset($_POST["emailPerfilEditar"]) && isset($_POST["contraseñaPerfilEditar"]) && 
			isset($_POST["usernamePerfilEditar"]) && isset($_POST["rolPerfilEditar"]) ) {

		
			if (preg_match('/^[a-zA-Z0-9]*$/', $_POST["nombrePerfilEditar"]) && 
				preg_match('/^[a-zA-Z0-9]*$/', $_POST["apellidoPerfilEditar"]) && 
				preg_match('/^[a-zA-Z0-9]*$/', $_POST["usernamePerfilEditar"]) && 
				preg_match('/^[a-zA-Z0-9]*$/', $_POST["rolPerfilEditar"]) && 
				preg_match('/^[a-zA-Z0-9]*$/', $_POST["contraseñaPerfilEditar"]) && 
				preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/' , $_POST["emailPerfilEditar"]) ) {

				$encriptar = crypt($_POST["contraseñaPerfilEditar"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

				if ($_POST["idEditarPerfil"] != "" ) {
					$id = $_POST["idEditarPerfil"];

				}else{
					$id = $_SESSION["id"];

				}

				$datosController = array('nombre' => $_POST["nombrePerfilEditar"] ,
										'apellido' => $_POST["apellidoPerfilEditar"] ,
										'email' => $_POST["emailPerfilEditar"] ,
										'username' => $_POST["usernamePerfilEditar"] ,
										'rol' => $_POST["rolPerfilEditar"] ,
										'id' => $id ,
										'contraseña' => $encriptar);
				// var_dump($datosController);
				$response = GestorPerfilesM::updatePerfilModel($datosController, "users");
			
				
			    if ($response == "ok") {

			    	if ($_POST["idEditarPerfil"] == "" ) {

						$dataUser = GestorPerfilesM::getdataUserModel($_POST["usernamePerfilEditar"], "users");

						foreach ($dataUser as $row => $item) {
							if ($item["rol"] == 0) {
								$rol = "Administrador";
							}else{
								$rol = "Editor";
							}
							$_SESSION["nombre"] = $item["nombre"];
							$_SESSION["apellido"] = $item["apellido"];
							$_SESSION["username"] = $item["username"];
							$_SESSION["rol"] = $rol;
							$_SESSION["email"] = $item["email"];
							$_SESSION["id"] = $item["id"];
						}

					}else{
						

					}

            		echo '
					<script>

							localStorage.setItem("nombre", "'.$_SESSION["nombre"].'");
							localStorage.setItem("apellido", "'.$_SESSION["apellido"].'");
							localStorage.setItem("username", "'.$_SESSION["username"].'");
							localStorage.setItem("identificador", "'.$_SESSION["id"].'");
							localStorage.setItem("email", "'.$_SESSION["email"].'");

						 swal({
                                title: "ok",
                                text: "Perfil Actualizado Correctamente!",
                                type: "success",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "perfiles";
                                } 
                            });


					</script>

            	';
	            }else{
	            	echo '
					<script>
						 swal({
                                title: "Error",
                                text: "Por favor trate nuevamente!",
                                type: "success",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "perfiles";
                                } 
                            });


					</script>

            	';
	            }

			}

		}
	}

	//MOSTRAR PERFILES
	public function viewsPerfilesController(){
		$response = GestorPerfilesM::viewsPerfilesModel($_SESSION["id"], "users");
		foreach ($response as $row => $item) {
			if ($item["rol"] == 0) {
				$rol = "Administrador";
			}else{
				$rol = "Editor";
			}
			echo '
				<tr>
					<td>'.$item["nombre"].'</td>
					<td>'.$item["apellido"].'</td>
					<td>'.$item["email"].'</td>
					<td>'.$item["username"].'</td>
					<td>'.$rol.'</td>
					<td class="action">

						<a class="btn btn-danger btnEliminarPerfil" href="index.php?action=perfiles&deleteP='.$item["id"].'" onclick="eliminar();return false;">Eliminar</a>

						<button type="button" class="btn btn-dark btnActualizarPerfil" data-toggle="modal" data-target="#modalEditarPerfil">Editar</button>

					</td>
				</tr>

			';
		}
	}

	//ELIMINAR PERFIL
	public function deletePerfilController(){
		if (isset($_GET["deleteP"])) {
			$response = GestorPerfilesM::deletePerfilModel($_GET["deleteP"], "users");
			if ($response == "ok" ) {
				echo '
					<script>
						 swal({
                                title: "OK",
                                text: "Perfil Eliminado Correctamente!",
                                type: "success",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "perfiles";
                                } 
                            });


					</script>';
			}else{

			}
		}
	}
} 