<?php 

class GestorProductosController{

	public function setProductosController(){

		if (isset($_POST["nombreProducto"]) &&
			isset($_POST["descripcionProducto"]) &&
			isset($_POST["precioProducto"]) &&
			isset($_POST["margenProducto"]) &&
			isset($_POST["numRojoProducto"]) &&
			isset($_POST["areaProducto"]) ) {
			
			if (preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["nombreProducto"] ) &&
				preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["descripcionProducto"] ) && 
				preg_match('/^[0-9 .]*$/', $_POST["precioProducto"] ) && 
				preg_match('/^[0-9 .]*$/', $_POST["margenProducto"] ) && 
				preg_match('/^[0-9]*$/', $_POST["numRojoProducto"] ) && 
				preg_match('/^[0-9]*$/', $_POST["areaProducto"] ) ) {
				
				$datosController = array('nombre' => $_POST["nombreProducto"],
										'descripcion' => $_POST["descripcionProducto"], 
										'precio' => $_POST["precioProducto"],
										'margen' => $_POST["margenProducto"],
										'rojos' => $_POST["numRojoProducto"],
										'idArea' => $_POST["areaProducto"] );

				$response = GestorProductosModel::setProductosModel($datosController, "productos");

				if ($response == "ok") {
					echo '
					<script>
						 swal({
                                title: "ok",
                                text: "Producto Registrado Correctamente!",
                                type: "success",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "productos";
                                } 
                            });


					</script>

            	';
				}else{
						echo '
					<script>
						 swal({
                                title: "Error",
                                text: "Por favor trate nuevamente!",
                                type: "success",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "productos";
                                } 
                            });


					</script>

            	';
	            
				}

			}
		}
	}

	public function getProductosController(){
		$response = GestorProductosModel::getProductosModel("productos");
		foreach ($response as $row => $item) {
			echo '
				<tr>
					<td>'.$item["nombre"].'</td>
					<td>'.$item["descripcion"].'</td>
					<td>$'.$item["precioV"].'</td>
					<td>'.$item["existencias"].'</td>
					<td>'.$item["margen"].'</td>
					<td>$'.$item["inversion"].'</td>
					<td>$'.$item["ganancia"].'</td>
					<td>'.$item["area"].'</td>
					<td>
						<button type="button" class="btn btn-success btnEditarProducto '.$item["numeroRojo"].'" data-toggle="modal" data-target="#modalEditarProducto">Editar</button>
						<a href="index.php?action=productos&deleteProducto='.$item["id"].'" class="btn btn-danger" onclick="eliminarProducto(this.href);return false;" >Eliminar</a>
					</td>
				</tr>
			';
		}
	}

	public function updateProductosController(){

		if (isset($_POST["nombreProductoEditar"]) &&
			isset($_POST["descripcionProductoEditar"]) &&
			isset($_POST["precioProductoEditar"]) &&
			isset($_POST["margenProductoEditar"]) &&
			isset($_POST["numRojoProductoEditar"]) &&
			isset($_POST["areaProductoEditar"]) ) {
			
			if (preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["nombreProductoEditar"] ) &&
				preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["descripcionProductoEditar"] ) && 
				preg_match('/^[0-9 .]*$/', $_POST["precioProductoEditar"] ) && 
				preg_match('/^[0-9 .]*$/', $_POST["margenProductoEditar"] ) && 
				preg_match('/^[0-9]*$/', $_POST["numRojoProductoEditar"] ) && 
				preg_match('/^[0-9]*$/', $_POST["areaProductoEditar"] ) ) {
				
				$datosController = array('nombre' => $_POST["nombreProductoEditar"],
										'descripcion' => $_POST["descripcionProductoEditar"], 
										'precio' => $_POST["precioProductoEditar"],
										'margen' => $_POST["margenProductoEditar"],
										'rojos' => $_POST["numRojoProductoEditar"],
										'idArea' => $_POST["areaProductoEditar"],
										'id' => $_POST["idProductoEditar"] );

				$response = GestorProductosModel::updateProductosModel($datosController, "productos");

				if ($response == "ok") {
					echo '
					<script>
						 swal({
                                title: "ok",
                                text: "Producto Actualizado Correctamente!",
                                type: "success",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "productos";
                                } 
                            });


					</script>

            	';
				}else{
						echo '
					<script>
						 swal({
                                title: "Error",
                                text: "Por favor trate nuevamente!",
                                type: "success",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "productos";
                                } 
                            });


					</script>

            	';
	            
				}

			}
		}else{
			echo "Error ";
		}
	}

	public function deleteProductosController(){
		if (isset($_GET["deleteProducto"])) {
			$response = GestorProductosModel::deleteProductosModel($_GET["deleteProducto"], "productos");
			if ($response == "ok") {
					echo '
					<script>
						 swal({
                                title: "ok",
                                text: "Producto Eliminado Correctamente!",
                                type: "success",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "productos";
                                } 
                            });


					</script>

            	';
			}else{

			}
		}
	}

	public function traerProductosParaGraficaController(){
		$response = GestorProductosModel::traerProductosParaGraficaModel("productos");
		foreach ($response as $row => $item) {
			echo '
				<option value="'.$item["id"].'">'.$item["nombre"].'</option>
			';
		}
	}

}
